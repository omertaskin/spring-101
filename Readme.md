#Build

`./mvnw clean install`

# Run

ApiApplication içindeki main method vasıtasıyla uygulama ayağa kaldırılabilir


product filter kuralları:

- ürün isimleri içerisinde 0 geçmemeli
- ürün isimleri min 3 karakter olmalı
- ürün isimleri max 20 karakter olmalı
- ürün isimleri ğ ile başlamamalı
