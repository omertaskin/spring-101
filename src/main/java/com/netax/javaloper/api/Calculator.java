package com.netax.javaloper.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Calculator {

  @Autowired
  private String companyName;
  // type ı kontrol et match eden 1 tane varsa dön
  // 1 den fazla varsa isme göre

  // Vat => Value Added Tax = KDV :D
  public double calculateVat(double price) {
    if (companyName.startsWith("NETAX")) {
      return 0d;
    }

    return price * 18 / 100;
  }


}
