package com.netax.javaloper.api.repository;

import com.netax.javaloper.api.entity.UserEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserEntity, Integer> {

  UserEntity findByName(String name);

  // HQL -> Hibernate Query Language
  @Query("select user from UserEntity as user where user.name = :neym")
  UserEntity durBiseyDenicem(String neym);
}
