package com.netax.javaloper.api.repository;

import com.netax.javaloper.api.entity.ProductEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProductRepository extends CrudRepository<ProductEntity, Integer> {

  List<ProductEntity> findByNameStartsWith(String keyword);
}
