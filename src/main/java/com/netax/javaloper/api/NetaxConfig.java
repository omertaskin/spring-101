package com.netax.javaloper.api;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class NetaxConfig {

  @Bean
  public String companyName() {
    return "NETAX LTD ŞTİ.";
  }

  @Bean
  public String founder() {
    return "Mehmet Demir";
  }
}
