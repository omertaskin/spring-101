package com.netax.javaloper.api.service;

public interface ProductFilterService {
  void filter(String productName);
}
