package com.netax.javaloper.api.service.impl;

import com.netax.javaloper.api.controller.response.Product;
import com.netax.javaloper.api.controller.response.User;
import com.netax.javaloper.api.entity.ProductEntity;
import com.netax.javaloper.api.repository.ProductRepository;
import com.netax.javaloper.api.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

  @Autowired
  private ProductRepository productRepository;

  @Override
  public List<Product> list() {
    Iterable<ProductEntity> productEntities = productRepository.findAll();
    List<Product> products = new ArrayList<>();

    productEntities.forEach(item -> {
      Product product = new Product();
      product.setId(item.getId());
      product.setName(item.getName());

      if (item.getUser() != null) {
        User user = new User();
        user.setName(item.getUser().getName());
        user.setSurname(item.getUser().getSurname());
        user.setFullName(item.getUser().getName() + " " + item.getUser().getSurname());

        product.setUser(user);
      }

      products.add(product);
    });

    return products;
  }

  @Override
  public List<Product> getProductsStartsWith(String keyword) {
    List<ProductEntity> productEntities = productRepository.findByNameStartsWith(keyword);
    List<Product> products = new ArrayList<>();

    if (keyword.length() < 2) return products;

    productEntities.forEach(item -> {
      Product product = new Product();
      product.setId(item.getId());
      product.setName(item.getName());

      if (item.getUser() != null) {
        User user = new User();
        user.setName(item.getUser().getName());
        user.setSurname(item.getUser().getSurname());
        user.setFullName(item.getUser().getName() + " " + item.getUser().getSurname());

        product.setUser(user);
      }

      products.add(product);
    });

    return products;
  }
}
