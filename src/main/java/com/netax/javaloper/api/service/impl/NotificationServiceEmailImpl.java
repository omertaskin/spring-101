package com.netax.javaloper.api.service.impl;

import com.netax.javaloper.api.service.NotificationService;
import org.springframework.stereotype.Service;

@Service
public class NotificationServiceEmailImpl implements NotificationService {

  @Override
  public void sendNotification(String notification) {
    System.out.println("email ile gonderilecek : " + notification);
  }
}
