package com.netax.javaloper.api.service.impl;

import com.netax.javaloper.api.service.ProductFilterService;
import org.springframework.stereotype.Component;

@Component
public class ProductFilterServiceSoftGImpl implements ProductFilterService {

  @Override
  public void filter(String productName) {
    if (productName.startsWith("ğ")) {
      throw new RuntimeException("ğ ile başlayamaz");
    }

    if (productName.startsWith("Ğ")) {
      throw new RuntimeException("ğ ile başlayamaz");
    }
  }
}
