package com.netax.javaloper.api.service;

import com.netax.javaloper.api.controller.response.Product;

import java.util.List;

// ne yapilacak
public interface ProductService {

  List<Product> list();

  List<Product> getProductsStartsWith(String keyword);
}
