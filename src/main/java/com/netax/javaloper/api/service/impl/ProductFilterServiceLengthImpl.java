package com.netax.javaloper.api.service.impl;

import com.netax.javaloper.api.service.ProductFilterService;
import org.springframework.stereotype.Component;

@Component
public class ProductFilterServiceLengthImpl implements ProductFilterService {

  @Override
  public void filter(String productName) {
    if (productName.length() < 3) {
      throw new RuntimeException("en az 3 karakter olmalı");
    }

    if (productName.length() > 20) {
      throw new RuntimeException("max 20 karakter olmalı");
    }
  }
}
