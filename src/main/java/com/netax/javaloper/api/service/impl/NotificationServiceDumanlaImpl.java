package com.netax.javaloper.api.service.impl;

import com.netax.javaloper.api.service.NotificationService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("enSevdigimHaberlesmeYontemi")
public class NotificationServiceDumanlaImpl implements NotificationService {
  /*
  Autowired
  {
    ProductService: ProductServiceImpl
    NotificationService: [NotificationServiceDumanlaImpl, NotificationServiceEmailImpl, NotificationServiceSmsImpl]
  }
  */

  @Override
  public void sendNotification(String notification) {
    System.out.println("dumanla gonderilecek " + notification);
  }
}
