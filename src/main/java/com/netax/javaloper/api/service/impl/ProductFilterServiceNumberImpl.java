package com.netax.javaloper.api.service.impl;

import com.netax.javaloper.api.service.ProductFilterService;
import org.springframework.stereotype.Component;

@Component
public class ProductFilterServiceNumberImpl implements ProductFilterService {

  @Override
  public void filter(String productName) {
    if (productName.indexOf('0') >= 0) {
      throw new RuntimeException("Ürün içinde numerik değer var!");
    }
  }
}
