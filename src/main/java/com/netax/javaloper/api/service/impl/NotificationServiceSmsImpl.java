package com.netax.javaloper.api.service.impl;

import com.netax.javaloper.api.service.NotificationService;
import org.springframework.stereotype.Service;

@Service
public class NotificationServiceSmsImpl implements NotificationService {

  @Override
  public void sendNotification(String notification) {
    System.out.println("sms ile gonderilecek : " + notification);
  }
}
