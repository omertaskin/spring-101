package com.netax.javaloper.api.controller;

import com.netax.javaloper.api.controller.request.UserRequest;
import com.netax.javaloper.api.entity.UserEntity;
import com.netax.javaloper.api.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {

  @Autowired
  private UserRepository userRepository;

  @GetMapping
  public Iterable<UserEntity> getUsers() {
    return userRepository.findAll();
  }

  @GetMapping("/{name}")
  public UserEntity getUser(@PathVariable String name) {
    return userRepository.durBiseyDenicem(name);
  }

  @PostMapping
  public Boolean save(@RequestBody UserRequest userRequest) {
    UserEntity userEntity = new UserEntity();
    userEntity.setName(userRequest.getName());
    userEntity.setSurname(userRequest.getSurname());


    if( userRequest.getName() == null || userRequest.getSurname() == null) return false;

    userRepository.save(userEntity);
    return true;
  }
}
