package com.netax.javaloper.api.controller;

import com.netax.javaloper.api.controller.request.ProductRequest;
import com.netax.javaloper.api.controller.response.Product;
import com.netax.javaloper.api.controller.response.ProductResponse;
import com.netax.javaloper.api.entity.ProductEntity;
import com.netax.javaloper.api.repository.ProductRepository;
import com.netax.javaloper.api.service.NotificationService;
import com.netax.javaloper.api.service.ProductFilterService;
import com.netax.javaloper.api.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductController {

  @Autowired
  private ProductRepository productRepository;

  @Autowired
  private ProductService productService;

  // Yöntem 1:
  @Autowired
  private NotificationService notificationServiceDumanlaImpl;

  // Yöntem 2:
  @Autowired
  @Qualifier("enSevdigimHaberlesmeYontemi")
  private NotificationService notificationService;

  // Yöntem 3:
  @Autowired
  private List<NotificationService> notificationServices;

  @Autowired
  private List<ProductFilterService> productFilterServices;

  @GetMapping
  public List<Product> list() {
    notificationServiceDumanlaImpl.sendNotification("yontem 1");
    notificationService.sendNotification("yontem 2");

    notificationServices.forEach(service -> service.sendNotification("YÖNTEM 3"));

    return productService.list();
  }

  @GetMapping("/test")
  public ProductResponse getProduct() {
    ProductResponse response = new ProductResponse();

    response.setId(1);
    response.setName("iphone 7");

    return response;
  }

  @PostMapping
  public ProductResponse save(@RequestBody ProductRequest request) {
    productFilterServices.forEach(service -> service.filter(request.getName()));

    ProductEntity entity = new ProductEntity();
    entity.setName(request.getName());

    productRepository.save(entity);
    return null;
  }


  @GetMapping("/query") // ?q=dşlaksjdşlakdsj
  public List<Product> getProductsStartsWith(@RequestParam("q") String keyword) {
    return productService.getProductsStartsWith(keyword);
  }

}
