package com.netax.javaloper.api;

import com.netax.javaloper.api.controller.response.UserResponse;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

@Repository
public class XUserRepository {

  public List<UserResponse> getUsers() {
    return Collections.singletonList(new UserResponse());
  }

}
