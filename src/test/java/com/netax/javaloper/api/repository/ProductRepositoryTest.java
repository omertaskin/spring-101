package com.netax.javaloper.api.repository;

import com.netax.javaloper.api.entity.ProductEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class ProductRepositoryTest {

  @Autowired
  private ProductRepository productRepository;

  @Test
  void findByNameStartsWith() {
    // test verisini oluşturmak
    ProductEntity productEntity = new ProductEntity();
    productEntity.setName("kolonya");
    productRepository.save(productEntity);

    // method call
    List<ProductEntity> products = productRepository.findByNameStartsWith("kol");

    // assertion
    Assertions.assertTrue(products.size() > 0);
  }
}