package com.netax.javaloper.api;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class CalculatorTest {

  @Autowired
  private Calculator calculator;

  @Autowired
  private String companyName;

  @Autowired
  private String founder;


  @Test
  void calculateVat() {
    System.err.println(companyName + " / " + founder);
    Assertions.assertEquals(0d, calculator.calculateVat(100d));
  }
}