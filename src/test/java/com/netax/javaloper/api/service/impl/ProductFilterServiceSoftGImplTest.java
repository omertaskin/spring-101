package com.netax.javaloper.api.service.impl;

import com.netax.javaloper.api.service.ProductFilterService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ProductFilterServiceSoftGImplTest {

  @Autowired
  private ProductFilterService productFilterServiceSoftGImpl;

  @Test
  void filterWithValidProductName() {
    productFilterServiceSoftGImpl.filter("üğün");
  }

  @Test
  void filterWithValidProductName_startsWithUpperCaseSoftG() {
    try {
      productFilterServiceSoftGImpl.filter("ĞÜN");
      Assertions.fail();
    } catch (Exception e) {
      System.err.println(e.toString());
    }
  }

  @Test
  void filterWithInvalidProductName() {
    try {
      productFilterServiceSoftGImpl.filter("ğün");
      Assertions.fail();
    } catch (Exception e) {
      System.err.println(e.toString());
    }
  }
}