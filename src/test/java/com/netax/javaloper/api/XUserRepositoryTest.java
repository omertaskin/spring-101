package com.netax.javaloper.api;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class XUserRepositoryTest {

  @Autowired
  private XUserRepository userRepository;

  @Test
  public void testGetUsers() {
    System.out.println(userRepository.getUsers());
  }
}